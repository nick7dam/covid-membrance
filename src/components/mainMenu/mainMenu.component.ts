import { Component, Vue } from 'vue-property-decorator'
@Component
export default class MainMenuComponent extends Vue {
public showMenu: boolean
constructor () {
  super()
  this.showMenu = true
}

public changeLang (lang: string) {
  if (lang) this.$i18n.locale = lang
}
}
