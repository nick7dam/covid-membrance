import { Component, Vue, Watch } from 'vue-property-decorator'
import axios, { AxiosResponse } from 'axios'
@Component({
  components: {}
})
export default class SlideDetailsComponent extends Vue {
  public slide: any
  public currentLang: string
  constructor () {
    super()
    this.slide = null
    this.currentLang = 'en'
  }

  @Watch('$route', { immediate: true, deep: true })
  public updateSlide (from: any) {
    const id = from.query ? from.query.id : ''
    axios.get('./config/slides.json').then((resp: AxiosResponse) => {
      this.slide = resp.data.find((item: any) => item.id === parseInt(id))
    })
  }

  @Watch('$i18n.locale', { immediate: true, deep: true })
  public updateLang (newVal: any) {
    this.currentLang = newVal
  }
}
