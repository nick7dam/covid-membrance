import { Component, Vue } from 'vue-property-decorator'
import carousel3d from '@/components/carousel/Carousel3d.vue'
import Slide from '@/components/carousel/Slide.vue'
import axios, { AxiosResponse } from 'axios'
@Component({
  components: {
    carousel3d,
    Slide
  }
})
export default class ExhibitionComponent extends Vue {
  public slides: any[]
  public selectedSlide: any
  public showGallery: boolean
  constructor () {
    super()
    this.slides = []
    this.showGallery = false
    this.selectedSlide = null
  }

  public mounted () {
    this.getSlides()
  }

  public getSlides () {
    axios.get('./config/exhibitions.json').then((resp: AxiosResponse) => {
      this.slides = resp.data
    })
  }

  public enterSlide (sl: any) {
    const slide = this.slides[sl.index]
    if (slide.private) return
    this.selectedSlide = slide
    this.showGallery = true
  }

  public selectNextSlide () {
    if (this.selectedSlide.id < this.slides.length) {
      const index = this.selectedSlide.id
      this.selectedSlide = this.slides[index]
    } else {
      this.selectedSlide = this.slides[0]
    }
  }

  public selectPreviousSlide () {
    if (this.selectedSlide.id > 1) {
      const index = this.selectedSlide.id
      this.selectedSlide = this.slides[index - 2]
    } else {
      this.selectedSlide = this.slides[this.slides.length - 1]
    }
  }
}
