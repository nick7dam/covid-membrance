import { Component, Vue, Watch } from 'vue-property-decorator'
import axios, { AxiosResponse } from 'axios'
@Component({
  components: {}
})
export default class MyStoryComponent extends Vue {
  public currentTab: string
  public currentLang: string
  public myVideos: any[]
  public covidDiaryNotes: any[]
  constructor () {
    super()
    this.myVideos = []
    this.covidDiaryNotes = []
    this.currentTab = 'myVideos'
    this.currentLang = 'ен'
  }

  public mounted () {
    this.getMyVideos()
    this.getCovidDiaryNotes()
  }

  public getMyVideos () {
    axios.get('./config/myVideos.json').then((resp: AxiosResponse) => {
      this.myVideos = resp.data
    })
  }

  public getCovidDiaryNotes () {
    axios.get('./config/diaryNotes.json').then((resp: AxiosResponse) => {
      this.covidDiaryNotes = resp.data
    })
  }

  @Watch('$i18n.locale', { immediate: true, deep: true })
  public updateLang (newVal: any) {
    this.currentLang = newVal
  }

  @Watch('$route', { immediate: true, deep: true })
  public updateRoute (from: any) {
    if (from.query && from.query.activeTab) {
      this.currentTab = 'inMyWords'
    }
  }

  public goToDiaryDetails (item: any) {
    this.$router.push(`/CovidDiaryDetails?id=${item.id}`)
  }
}
