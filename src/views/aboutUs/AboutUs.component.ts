import { Component, Vue, Watch } from 'vue-property-decorator'
import axios, { AxiosResponse } from 'axios'
@Component({
  components: {}
})
export default class AboutUsComponent extends Vue {
  public currentLang: string
  public aboutUs: any
  constructor () {
    super()
    this.currentLang = 'en'
    this.aboutUs = null
  }

  public mounted () {
    this.getAboutUs()
  }

  public getAboutUs () {
    axios.get('./config/aboutUs.json').then((resp: AxiosResponse) => {
      this.aboutUs = resp.data
    })
  }

  @Watch('$i18n.locale', { immediate: true, deep: true })
  public updateLang (newVal: any) {
    this.currentLang = newVal
  }
}
